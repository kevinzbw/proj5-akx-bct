from simple_page_rank import SimplePageRank

"""
This class implements the pagerank algorithm with
backwards edges as described in the second part of 
the project.
"""
class BackedgesPageRank(SimplePageRank):

    """
    The implementation of __init__ and compute_pagerank should 
    still be the same as SimplePageRank.
    You are free to override them if you so desire, but the signatures
    must remain the same.
    """

    """
    This time you will be responsible for implementing the initialization
    as well. 
    Think about what additional information your data structure needs 
    compared to the old case to compute weight transfers from pressing
    the 'back' button.
    """
    @staticmethod
    def initialize_nodes(input_rdd):
        # YOUR CODE HERE
        # The pattern that this solution uses is to keep track of 
        # (node, (weight, targets, old_weight)) for each iteration.
        # When calculating the score for the next iteration, you
        # know that 10% of the score you sent out from the previous
        # iteration will get sent back.
        def emit_edges(line):
            # ignore blank lines and comments
            if len(line) == 0 or line[0] == "#":
                return []
            # get the source and target labels
            source, target = tuple(map(int, line.split()))
            # emit the edge
            edge = (source, frozenset([target]))
            # also emit "empty" edges to catch nodes that do not have any
            # other node leading into them, but we still want in our list of nodes
            self_source = (source, frozenset())
            self_target = (target, frozenset())
            return [edge, self_source, self_target]
            """
            0(1) 0() 0()
            0(2) 0() 2()
            2(3) 2() 3()
            """
        def reduce_edges(e1, e2):
            return e1 | e2

        def initialize_weights((source, targets)):
            return (source, (1.0, targets, 1.0))

        nodes = input_rdd\
                .flatMap(emit_edges)\
                .reduceByKey(reduce_edges)
        
        num = nodes.count()
        nodes = nodes.map(initialize_weights)

        return nodes

    """
    You will also implement update_weights and format_output from scratch.
    You may find the distribute and collect pattern from SimplePageRank
    to be suitable, but you are free to do whatever you want as long
    as it results in the correct output.
    """
    @staticmethod
    def update_weights(nodes, num_nodes):
        # YOUR CODE HERE
        def distribute_weights((node, (weight, targets, old_weight))):
            # YOUR CODE HERE
            # print node, old_weight

            # ret = []
            # n = len(targets)
            # print old_weight,targets


            # for target in xrange(num_nodes):
            #     if target != node:
            #         if n != 0:
            #             if target in targets:
            #                 old = old_weight.get(target)
            #                 if not old:
            #                     old = 0
            #                 value = weight * 0.85 / n + old * 0.1
            #                 ret.append((target, (value, frozenset(), node)))
            #             else:
            #                 old = old_weight.get(target)
            #                 if not old:
            #                     old = 0
            #                 value = old * 0.1
            #                 ret.append((target, (value, frozenset(), node)))
            #         if n == 0:
            #             old = old_weight.get(target)
            #             if not old:
            #                 old = 0
            #             value = weight * 0.85 / (num_nodes - 1) + old * 0.1
            #             ret.append((target, (value, frozenset(), node)))
            #     else:
            #         if node not in targets:
            #             old = old_weight.get(target)
            #             if not old:
            #                 old = 0
            #             value = weight * 0.05 + old * 0.1
            #             ret.append((node, (value, targets, node)))
            #         else:
            #             old = old_weight.get(target)
            #             if not old:
            #                 old = 0
            #             value = weight * 0.05 + weight * 0.85 / n + old * 0.1
            #             ret.append((node, (value, targets, node)))
            
            # targets_set = set(targets)
            # if node not in targets:
            #     old = old_weight.get((node,node))
            #     value = weight * 0.05 + old * 0.1
            #     ret.append((node, (value, targets_set, (node,node))))
            # else:
            #     old = old_weight.get((node,node))
            #     value = weight * 0.05 + weight * 0.85 / n + old * 0.1
            #     ret.append((node, (value, targets_set, (node,node))))

            # if n != 0:
            #     for target in targets:
            #         if target != node:
            #             value = weight * 0.85 / n
            #             ret.append((target, (value, frozenset(), (node, target))))
                        
            #             old = old_weight.get((node, target))
            #             if old:
            #                 value = old * 0.1
            #                 ret.append((node, (value, frozenset(), (target, node))))

            # else:
            #     for target in xrange(num_nodes):
            #         if target != node:
            #             value = weight * 0.85 / (num_nodes - 1)
            #             ret.append((target, (value, frozenset(), (node, target))))
            #             old = old_weight.get((node, target))
            #             if old:
            #                 value = old * 0.1
            #                 ret.append((node, (value, frozenset(), (target, node))))

            
            ret = []
            n = len(targets)
            #print node, weight, targets
            if node not in targets:
                value = weight * 0.05 + old_weight * 0.1
                ret.append((node, (value, targets, weight)))
            else:
                value = weight * 0.05 + weight * 0.85 / n + old_weight * 0.1
                ret.append((node, (value, targets, weight)))

            if n != 0:
                for target in targets:
                    if target != node:
                        ret.append((target, (weight * 0.85 / n , frozenset(), 0)))
            else:
                for target in xrange(num_nodes):
                    if target != node:
                        ret.append((target, (weight * 0.85 / (num_nodes - 1), frozenset(), 0)))
            return ret  

            # for t, (v1,s,fn) in ret:
            #     print node, t, v1
            # print 
            # return ret     

        def collect_weights((node, pairs)):
            s = 0
            old_weight = 0
            t = frozenset()
            for value, targets, old in pairs:
                s += value
                if len(targets) != 0:
                    t = targets
                if old != 0:
                    old_weight = old
            return (node, (s, t, old_weight))


        return nodes\
                .flatMap(distribute_weights)\
                .groupByKey()\
                .map(collect_weights)
                     
    @staticmethod
    def format_output(nodes): 
        # YOUR CODE HERE
        return nodes\
                .map(lambda (node, (weight, targets, old_weight)): (weight, node))\
                .sortByKey(ascending = False)\
                .map(lambda (weight, node): (node, weight))